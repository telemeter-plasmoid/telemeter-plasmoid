project(plasma-applet-telemeter)

# Find the required Libaries
find_package(KDE4 REQUIRED)
find_package(OpenSSL REQUIRED)

add_definitions(-DWITH_OPENSSL)
#SET (CMAKE_SHARED_LINKER_FLAGS)
#     CACHE STRING "Flags used by the linker during the creation of dll's.")
#STRING(REPLACE "-Wl,--no-undefined" "" CMAKE_MODULE_LINKER_FLAGS ${CMAKE_MODULE_LINKER_FLAGS})

include(KDE4Defaults)

add_definitions (${QT_DEFINITIONS} ${KDE4_DEFINITIONS})
include_directories(
   ${CMAKE_SOURCE_DIR}
   ${CMAKE_BINARY_DIR}
   ${KDE4_INCLUDES}
   ${OPENSSL_INCLUDE_DIR}
   )

set(telemeter_SRCS
    gsoap/soapC.cpp
    gsoap/soapH.h
    gsoap/stdsoap2.cpp
    gsoap/soapStub.h
    gsoap/soapTelemeterServiceSOAPProxy.cpp
    gsoap/TelemeterServiceSOAP.nsmap
    telemeterplasmoid.cpp
    telemeterservice.cpp)

#list(APPEND CMAKE_CXX_FLAGS -DWITH_OPENSSL)
kde4_add_ui_files(telemeter_SRCS telemeterConfig.ui)

MESSAGE("ssl libs = ${OPENSSL_LIBRARIES}")
kde4_add_plugin(plasma_applet_telemeter ${telemeter_SRCS})
#SET_TARGET_PROPERTIES(plasma_applet_telemeter PROPERTIES LINK_FLAGS -Wl,--allow-shlib-undefined)
target_link_libraries(plasma_applet_telemeter
                      ${KDE4_PLASMA_LIBS} ${KDE4_KDEUI_LIBS} ${OPENSSL_LIBRARIES})

install(TARGETS plasma_applet_telemeter
        DESTINATION ${PLUGIN_INSTALL_DIR})

install(FILES plasma-applet-telemeter.desktop
        DESTINATION ${SERVICES_INSTALL_DIR})
install(FILES "analog_meter2.svgz" DESTINATION ${DATA_INSTALL_DIR}/desktoptheme/default/widgets/ )
kde4_install_icons( ${ICON_INSTALL_DIR} )
