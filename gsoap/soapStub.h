/* soapStub.h
   Generated by gSOAP 2.7.17 from telemeter.h
   Copyright(C) 2000-2010, Robert van Engelen, Genivia Inc. All Rights Reserved.
   This part of the software is released under one of the following licenses:
   GPL, the gSOAP public license, or Genivia's license for commercial use.
*/

#ifndef soapStub_H
#define soapStub_H
#define SOAP_NAMESPACE_OF_ns1	"http://www.telenet.be/TelemeterService/"
#include "stdsoap2.h"

/******************************************************************************\
 *                                                                            *
 * Enumerations                                                               *
 *                                                                            *
\******************************************************************************/


#ifndef SOAP_TYPE_ns1__UnitType
#define SOAP_TYPE_ns1__UnitType (22)
/* ns1:UnitType */
enum ns1__UnitType {ns1__UnitType__MB = 0, ns1__UnitType__GB = 1};
#endif

/******************************************************************************\
 *                                                                            *
 * Types with Custom Serializers                                              *
 *                                                                            *
\******************************************************************************/


/******************************************************************************\
 *                                                                            *
 * Classes and Structs                                                        *
 *                                                                            *
\******************************************************************************/


#if 0 /* volatile type: do not declare here, declared elsewhere */

#endif

#ifndef SOAP_TYPE_ns1__RetrieveUsageRequestType
#define SOAP_TYPE_ns1__RetrieveUsageRequestType (10)
/* ns1:RetrieveUsageRequestType */
class SOAP_CMAC ns1__RetrieveUsageRequestType
{
public:
	char *UserId;	/* required element of type xsd:string */
	char *Password;	/* required element of type xsd:string */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 10; } /* = unique id SOAP_TYPE_ns1__RetrieveUsageRequestType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__RetrieveUsageRequestType(): UserId(NULL), Password(NULL), soap(NULL) { ns1__RetrieveUsageRequestType::soap_default(NULL); }
	virtual ~ns1__RetrieveUsageRequestType() { }
};
#endif

#ifndef SOAP_TYPE__ns1__union_RetrieveUsageResponseType
#define SOAP_TYPE__ns1__union_RetrieveUsageResponseType (29)
/* xsd:choice */
union _ns1__union_RetrieveUsageResponseType
{
#define SOAP_UNION__ns1__union_RetrieveUsageResponseType_Volume	(1)
	class ns1__VolumeType *Volume;
#define SOAP_UNION__ns1__union_RetrieveUsageResponseType_Stage	(2)
	class ns1__StageType *Stage;
};
#endif

#ifndef SOAP_TYPE_ns1__RetrieveUsageResponseType
#define SOAP_TYPE_ns1__RetrieveUsageResponseType (11)
/* ns1:RetrieveUsageResponseType */
class SOAP_CMAC ns1__RetrieveUsageResponseType
{
public:
	class ns1__TicketType *Ticket;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* required element of type ns1:TicketType */
	int __union_RetrieveUsageResponseType;	/* union discriminant (of union defined below) */
	union _ns1__union_RetrieveUsageResponseType union_RetrieveUsageResponseType;	/* required element of type xsd:choice */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 11; } /* = unique id SOAP_TYPE_ns1__RetrieveUsageResponseType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__RetrieveUsageResponseType(): Ticket(NULL), soap(NULL) { ns1__RetrieveUsageResponseType::soap_default(NULL); }
	virtual ~ns1__RetrieveUsageResponseType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__RetrievePossibleStagesRequestType
#define SOAP_TYPE_ns1__RetrievePossibleStagesRequestType (12)
/* ns1:RetrievePossibleStagesRequestType */
class SOAP_CMAC ns1__RetrievePossibleStagesRequestType
{
public:
	char *UserId;	/* required element of type xsd:string */
	char *Password;	/* required element of type xsd:string */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 12; } /* = unique id SOAP_TYPE_ns1__RetrievePossibleStagesRequestType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__RetrievePossibleStagesRequestType(): UserId(NULL), Password(NULL), soap(NULL) { ns1__RetrievePossibleStagesRequestType::soap_default(NULL); }
	virtual ~ns1__RetrievePossibleStagesRequestType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__RetrievePossibleStagesResponseType
#define SOAP_TYPE_ns1__RetrievePossibleStagesResponseType (13)
/* ns1:RetrievePossibleStagesResponseType */
class SOAP_CMAC ns1__RetrievePossibleStagesResponseType
{
public:
	ns1__TicketType *Ticket;	/* SOAP 1.2 RPC return element (when namespace qualified) */	/* required element of type ns1:TicketType */
	class ns1__StageListType *StageList;	/* required element of type ns1:StageListType */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 13; } /* = unique id SOAP_TYPE_ns1__RetrievePossibleStagesResponseType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__RetrievePossibleStagesResponseType(): Ticket(NULL), StageList(NULL), soap(NULL) { ns1__RetrievePossibleStagesResponseType::soap_default(NULL); }
	virtual ~ns1__RetrievePossibleStagesResponseType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__TicketType
#define SOAP_TYPE_ns1__TicketType (14)
/* ns1:TicketType */
class SOAP_CMAC ns1__TicketType
{
public:
	time_t Timestamp;	/* required element of type xsd:dateTime */
	time_t ExpiryTimestamp;	/* required element of type xsd:dateTime */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 14; } /* = unique id SOAP_TYPE_ns1__TicketType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__TicketType(): soap(NULL) { ns1__TicketType::soap_default(NULL); }
	virtual ~ns1__TicketType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__VolumeType
#define SOAP_TYPE_ns1__VolumeType (15)
/* ns1:VolumeType */
class SOAP_CMAC ns1__VolumeType
{
public:
	enum ns1__UnitType Unit;	/* required element of type ns1:UnitType */
	char *Limit;	/* required element of type xsd:nonNegativeInteger */
	char *TotalUsage;	/* required element of type xsd:nonNegativeInteger */
	class ns1__DailyUsageListType *DailyUsageList;	/* required element of type ns1:DailyUsageListType */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 15; } /* = unique id SOAP_TYPE_ns1__VolumeType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__VolumeType(): Limit(NULL), TotalUsage(NULL), DailyUsageList(NULL), soap(NULL) { ns1__VolumeType::soap_default(NULL); }
	virtual ~ns1__VolumeType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__DailyUsageListType
#define SOAP_TYPE_ns1__DailyUsageListType (16)
/* ns1:DailyUsageListType */
class SOAP_CMAC ns1__DailyUsageListType
{
public:
	int __sizeDailyUsage;	/* sequence of elements <DailyUsage> */
	class ns1__DailyUsageType **DailyUsage;	/* optional element of type ns1:DailyUsageType */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 16; } /* = unique id SOAP_TYPE_ns1__DailyUsageListType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__DailyUsageListType(): DailyUsage(NULL), soap(NULL) { ns1__DailyUsageListType::soap_default(NULL); }
	virtual ~ns1__DailyUsageListType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__DailyUsageType
#define SOAP_TYPE_ns1__DailyUsageType (17)
/* ns1:DailyUsageType */
class SOAP_CMAC ns1__DailyUsageType
{
public:
	char *Day;	/* required element of type xsd:date */
	char *Usage;	/* required element of type xsd:nonNegativeInteger */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 17; } /* = unique id SOAP_TYPE_ns1__DailyUsageType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__DailyUsageType(): Day(NULL), Usage(NULL), soap(NULL) { ns1__DailyUsageType::soap_default(NULL); }
	virtual ~ns1__DailyUsageType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__StageListType
#define SOAP_TYPE_ns1__StageListType (18)
/* ns1:StageListType */
class SOAP_CMAC ns1__StageListType
{
public:
	int __sizeStage;	/* sequence of elements <Stage> */
	ns1__StageType **Stage;	/* optional element of type ns1:StageType */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 18; } /* = unique id SOAP_TYPE_ns1__StageListType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__StageListType(): Stage(NULL), soap(NULL) { ns1__StageListType::soap_default(NULL); }
	virtual ~ns1__StageListType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__StageType
#define SOAP_TYPE_ns1__StageType (19)
/* ns1:StageType */
class SOAP_CMAC ns1__StageType
{
public:
	char *StageNumber;	/* required element of type ns1:StageNumberType */
	class ns1__RGBColorType *Color;	/* required element of type ns1:RGBColorType */
	char *Description;	/* required element of type xsd:string */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 19; } /* = unique id SOAP_TYPE_ns1__StageType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__StageType(): StageNumber(NULL), Color(NULL), Description(NULL), soap(NULL) { ns1__StageType::soap_default(NULL); }
	virtual ~ns1__StageType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__RGBColorType
#define SOAP_TYPE_ns1__RGBColorType (20)
/* ns1:RGBColorType */
class SOAP_CMAC ns1__RGBColorType
{
public:
	char *Red;	/* required element of type ns1:ColorValueType */
	char *Green;	/* required element of type ns1:ColorValueType */
	char *Blue;	/* required element of type ns1:ColorValueType */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 20; } /* = unique id SOAP_TYPE_ns1__RGBColorType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__RGBColorType(): Red(NULL), Green(NULL), Blue(NULL), soap(NULL) { ns1__RGBColorType::soap_default(NULL); }
	virtual ~ns1__RGBColorType() { }
};
#endif

#ifndef SOAP_TYPE_ns1__TelemeterFaultType
#define SOAP_TYPE_ns1__TelemeterFaultType (21)
/* ns1:TelemeterFaultType */
class SOAP_CMAC ns1__TelemeterFaultType
{
public:
	char *Code;	/* required element of type xsd:string */
	char *Description;	/* required element of type xsd:string */
	ns1__TicketType *Ticket;	/* optional element of type ns1:TicketType */
	struct soap *soap;	/* transient */
public:
	virtual int soap_type() const { return 21; } /* = unique id SOAP_TYPE_ns1__TelemeterFaultType */
	virtual void soap_default(struct soap*);
	virtual void soap_serialize(struct soap*) const;
	virtual int soap_put(struct soap*, const char*, const char*) const;
	virtual int soap_out(struct soap*, const char*, int, const char*) const;
	virtual void *soap_get(struct soap*, const char*, const char*);
	virtual void *soap_in(struct soap*, const char*, const char*);
	         ns1__TelemeterFaultType(): Code(NULL), Description(NULL), Ticket(NULL), soap(NULL) { ns1__TelemeterFaultType::soap_default(NULL); }
	virtual ~ns1__TelemeterFaultType() { }
};
#endif

#ifndef SOAP_TYPE_SOAP_ENV__Detail
#define SOAP_TYPE_SOAP_ENV__Detail (37)
/* SOAP-ENV:Detail */
struct SOAP_ENV__Detail
{
public:
	ns1__TelemeterFaultType *ns1__TelemeterFault;	/* optional element of type ns1:TelemeterFaultType */
	int __type;	/* any type of element <fault> (defined below) */
	void *fault;	/* transient */
	char *__any;
};
#endif

#ifndef SOAP_TYPE___ns1__retrieveUsage
#define SOAP_TYPE___ns1__retrieveUsage (44)
/* Operation wrapper: */
struct __ns1__retrieveUsage
{
public:
	ns1__RetrieveUsageRequestType *ns1__RetrieveUsageRequest;	/* optional element of type ns1:RetrieveUsageRequestType */
};
#endif

#ifndef SOAP_TYPE___ns1__retrievePossibleStages
#define SOAP_TYPE___ns1__retrievePossibleStages (48)
/* Operation wrapper: */
struct __ns1__retrievePossibleStages
{
public:
	ns1__RetrievePossibleStagesRequestType *ns1__RetrievePossibleStagesRequest;	/* optional element of type ns1:RetrievePossibleStagesRequestType */
};
#endif

#ifndef SOAP_TYPE_SOAP_ENV__Header
#define SOAP_TYPE_SOAP_ENV__Header (49)
/* SOAP Header: */
struct SOAP_ENV__Header
{
#ifdef WITH_NOEMPTYSTRUCT
private:
	char dummy;	/* dummy member to enable compilation */
#endif
};
#endif

#ifndef SOAP_TYPE_SOAP_ENV__Code
#define SOAP_TYPE_SOAP_ENV__Code (50)
/* SOAP Fault Code: */
struct SOAP_ENV__Code
{
public:
	char *SOAP_ENV__Value;	/* optional element of type xsd:QName */
	struct SOAP_ENV__Code *SOAP_ENV__Subcode;	/* optional element of type SOAP-ENV:Code */
};
#endif

#ifndef SOAP_TYPE_SOAP_ENV__Reason
#define SOAP_TYPE_SOAP_ENV__Reason (52)
/* SOAP-ENV:Reason */
struct SOAP_ENV__Reason
{
public:
	char *SOAP_ENV__Text;	/* optional element of type xsd:string */
};
#endif

#ifndef SOAP_TYPE_SOAP_ENV__Fault
#define SOAP_TYPE_SOAP_ENV__Fault (53)
/* SOAP Fault: */
struct SOAP_ENV__Fault
{
public:
	char *faultcode;	/* optional element of type xsd:QName */
	char *faultstring;	/* optional element of type xsd:string */
	char *faultactor;	/* optional element of type xsd:string */
	struct SOAP_ENV__Detail *detail;	/* optional element of type SOAP-ENV:Detail */
	struct SOAP_ENV__Code *SOAP_ENV__Code;	/* optional element of type SOAP-ENV:Code */
	struct SOAP_ENV__Reason *SOAP_ENV__Reason;	/* optional element of type SOAP-ENV:Reason */
	char *SOAP_ENV__Node;	/* optional element of type xsd:string */
	char *SOAP_ENV__Role;	/* optional element of type xsd:string */
	struct SOAP_ENV__Detail *SOAP_ENV__Detail;	/* optional element of type SOAP-ENV:Detail */
};
#endif

/******************************************************************************\
 *                                                                            *
 * Typedefs                                                                   *
 *                                                                            *
\******************************************************************************/

#ifndef SOAP_TYPE__QName
#define SOAP_TYPE__QName (5)
typedef char *_QName;
#endif

#ifndef SOAP_TYPE__XML
#define SOAP_TYPE__XML (6)
typedef char *_XML;
#endif

#ifndef SOAP_TYPE_xsd__date
#define SOAP_TYPE_xsd__date (7)
typedef char *xsd__date;
#endif

#ifndef SOAP_TYPE_xsd__nonNegativeInteger
#define SOAP_TYPE_xsd__nonNegativeInteger (8)
typedef char *xsd__nonNegativeInteger;
#endif

#ifndef SOAP_TYPE_xsd__positiveInteger
#define SOAP_TYPE_xsd__positiveInteger (9)
typedef char *xsd__positiveInteger;
#endif

#ifndef SOAP_TYPE_ns1__StageNumberType
#define SOAP_TYPE_ns1__StageNumberType (23)
typedef char *ns1__StageNumberType;
#endif

#ifndef SOAP_TYPE_ns1__ColorValueType
#define SOAP_TYPE_ns1__ColorValueType (24)
typedef char *ns1__ColorValueType;
#endif


/******************************************************************************\
 *                                                                            *
 * Externals                                                                  *
 *                                                                            *
\******************************************************************************/


#endif

/* End of soapStub.h */
