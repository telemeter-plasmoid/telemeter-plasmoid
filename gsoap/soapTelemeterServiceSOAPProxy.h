/* soapTelemeterServiceSOAPProxy.h
   Generated by gSOAP 2.7.17 from telemeter.h
   Copyright(C) 2000-2010, Robert van Engelen, Genivia Inc. All Rights Reserved.
   This part of the software is released under one of the following licenses:
   GPL, the gSOAP public license, or Genivia's license for commercial use.
*/

#ifndef soapTelemeterServiceSOAPProxy_H
#define soapTelemeterServiceSOAPProxy_H
#include "soapH.h"

class SOAP_CMAC TelemeterServiceSOAPProxy : public soap
{ public:
	/// Endpoint URL of service 'TelemeterServiceSOAPProxy' (change as needed)
	const char *soap_endpoint;
	/// Constructor
	TelemeterServiceSOAPProxy();
	/// Constructor with copy of another engine state
	TelemeterServiceSOAPProxy(const struct soap&);
	/// Constructor with engine input+output mode control
	TelemeterServiceSOAPProxy(soap_mode iomode);
	/// Constructor with engine input and output mode control
	TelemeterServiceSOAPProxy(soap_mode imode, soap_mode omode);
	/// Destructor frees deserialized data
	virtual	~TelemeterServiceSOAPProxy();
	/// Initializer used by constructors
	virtual	void TelemeterServiceSOAPProxy_init(soap_mode imode, soap_mode omode);
	/// Delete all deserialized data (uses soap_destroy and soap_end)
	virtual	void destroy();
	/// Disables and removes SOAP Header from message
	virtual	void soap_noheader();
	/// Get SOAP Fault structure (NULL when absent)
	virtual	const SOAP_ENV__Fault *soap_fault();
	/// Get SOAP Fault string (NULL when absent)
	virtual	const char *soap_fault_string();
	/// Get SOAP Fault detail as string (NULL when absent)
	virtual	const char *soap_fault_detail();
	/// Force close connection (normally automatic, except for send_X ops)
	virtual	int soap_close_socket();
	/// Print fault
	virtual	void soap_print_fault(FILE*);
#ifndef WITH_LEAN
	/// Print fault to stream
	virtual	void soap_stream_fault(std::ostream&);
	/// Put fault into buffer
	virtual	char *soap_sprint_fault(char *buf, size_t len);
#endif

	/// Web service operation 'retrieveUsage' (returns error code or SOAP_OK)
	virtual	int retrieveUsage(ns1__RetrieveUsageRequestType *ns1__RetrieveUsageRequest, ns1__RetrieveUsageResponseType *ns1__RetrieveUsageResponse);

	/// Web service operation 'retrievePossibleStages' (returns error code or SOAP_OK)
	virtual	int retrievePossibleStages(ns1__RetrievePossibleStagesRequestType *ns1__RetrievePossibleStagesRequest, ns1__RetrievePossibleStagesResponseType *ns1__RetrievePossibleStagesResponse);
};
#endif
