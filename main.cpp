#include <QtGui/QApplication>

#include "telemeterwidget.h"
#include "config.h"

#include "gsoap/stdsoap2.h"
#include "gsoap/soapH.h"

#include "gsoap/TelemeterServiceSOAP.nsmap"

#include <QtDebug>
#include <signal.h>

void sigpipe_handle(int x)
{
    qDebug() << "sigpipe_handle " << x;
}

void initOpenSsl()
{
    soap_ssl_init(); /* init OpenSSL (just once) */
    signal(SIGPIPE, sigpipe_handle);
}

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Mattiesworld");
    QCoreApplication::setOrganizationDomain("mattiesworld.gotdns.org");
    QCoreApplication::setApplicationName("telemeter");
    config = new Config();

    initOpenSsl();
    QApplication a(argc, argv);
    TelemeterWidget w;
    w.setCredentials(config->user, config->password);
    w.show();

    int res = a.exec();
    delete config;
    return res;
}
