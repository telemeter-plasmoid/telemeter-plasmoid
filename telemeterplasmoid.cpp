#include "telemeterplasmoid.h"

#include "gsoap/TelemeterServiceSOAP.nsmap"

#include <QPainter>
#include <QFontMetrics>
#include <QSizeF>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QtConcurrentRun>
#include <QtCore/qmath.h>

#include <KConfigDialog>
#include <KConfigGroup>
#include <Plasma/ToolTipManager>

#include <signal.h>

using KWallet::Wallet;

TelemeterPlasmoid::TelemeterPlasmoid(QObject *parent, const QVariantList &args)
    : Plasma::Applet(parent, args),
      wallet(0),
      walletWait(None),
      fetchInProgress(false),
      errorCount(0)
{
    setBackgroundHints(NoBackground);
    setHasConfigurationInterface(true);
    setAspectRatioMode(Plasma::IgnoreAspectRatio);

    QGraphicsLinearLayout* layout = new QGraphicsLinearLayout();
    setLayout(layout);

    meter = new Plasma::Meter();
    meter->setSvg("widgets/analog_meter2");
    meter->setMeterType(Plasma::Meter::AnalogMeter);
    meter->setMaximum(100);
    meter->setLabel(0, i18n("init"));
    meter->setLabel(1, "Telemeter");
    meter->setLabelColor(0, Qt::black);
    meter->setLabelColor(1, Qt::black);
    layout->addItem(meter);

    tooltip.setAutohide(false);

    connect(&telemeterService, SIGNAL(usageFetched(float,float,QString)), this, SLOT(onUsageFetched(float,float,QString)));
    connect(&telemeterService, SIGNAL(fetchError(QString)), this, SLOT(onError(QString)));
    connect(&refreshTimer, SIGNAL(timeout()), this, SLOT(fetchUsageAsync()));

    resize(200, 200);
    fprintf(stdout, "O:telemeter plasmoid startup\n");
    fprintf(stderr, "E:telemeter plasmoid startup\n");
}


TelemeterPlasmoid::~TelemeterPlasmoid()
{
    if (hasFailedToLaunch()) {
        // Do some cleanup here
    } else {
        // Save settings
    }
}

void sigpipe_handle(int x)
{
    qDebug() << "sigpipe_handle " << x;
}

void TelemeterPlasmoid::init()
{
    kDebug() << "init";
    readConfig();
    soap_ssl_init(); /* init OpenSSL (just once) */
    signal(SIGPIPE, sigpipe_handle);
    if (!walletWait && !configurationRequired()) fetchUsageAsync();
}

void TelemeterPlasmoid::mousePressEvent ( QGraphicsSceneMouseEvent * event )
{
    event->accept();
}

void TelemeterPlasmoid::mouseReleaseEvent ( QGraphicsSceneMouseEvent * event )
{
    fetchUsageAsync();
}

void TelemeterPlasmoid::createConfigurationInterface(KConfigDialog *parent)
{
    QWidget *widget = new QWidget(parent);
    uiConfig.setupUi(widget);
    uiConfig.loginLE->setText(login);
    uiConfig.passwordLE->setText(pwd);
    connect(parent, SIGNAL(accepted()), SLOT(onConfigAccepted()));
    if (useKWallet) {
        uiConfig.kwalletRB->setChecked(true);
    } else {
        uiConfig.configFileRB->setChecked(true);
    }
    if (autoRefresh) {
        uiConfig.refreshAutoRB->setChecked(true);
    } else {
        uiConfig.refreshManualRB->setChecked(true);
    }
    if (displayGB) {
        uiConfig.sizeGBRB->setChecked(true);
    } else {
        uiConfig.sizeMBRB->setChecked(true);
    }
    parent->addPage(widget, i18n("General"), icon());
}

void TelemeterPlasmoid::fetchUsageAsync() {
    if (fetchInProgress) {
        qDebug() << "TelemeterPlasmoid::fetchUsageAsync: ignore refresh request, fetch in progress";
        return;
    }
    qDebug() << "TelemeterPlasmoid::fetchUsageAsync";
    fetchInProgress = true;
    QtConcurrent::run(&telemeterService, &TelemeterService::fetchUsage);
    meter->setValue(0);
    meter->setLabel(0, i18n("refreshing.."));
    meter->update();
}

void TelemeterPlasmoid::onUsageFetched(float volumeUsed, float volumeTotal, QString unit)
{
    qDebug() << "onUsageFetched";
    errorCount = 0;
    float usage = volumeUsed * 100 /volumeTotal;
    qDebug() << "usage = " << usage;
    QString usageStr;
    if (displayGB && unit == "MB") {
        usageStr = QString(i18n("%1 of %2 %3")).arg(QString::number(volumeUsed * 0.001, 'f', 2)).arg(QString::number(volumeTotal * 0.001, 'f', 1)).arg("GB");
    } else {
        usageStr = QString(i18n("%1 of %2 %3")).arg(volumeUsed).arg(volumeTotal).arg(unit);
    }
    meter->setValue(usage);
    meter->setLabel(0, usageStr);
    meter->update();
    int nextReq; //secs
    if (autoRefresh) {
        nextReq = telemeterService.timestampRequest.secsTo(telemeterService.timestampExpiry);
    } else {
        nextReq = refreshInterval * 60;
    }
    qDebug() << "nextReq" << nextReq;
    refreshTimer.start(nextReq * 1000);
    QDateTime nextRefresh = QDateTime::currentDateTime();
    nextRefresh = nextRefresh.addSecs(nextReq);
    qDebug() << "next refresh" << nextRefresh;
    tooltip.setMainText("Status OK");
    tooltip.setSubText(QString("Usage: %1<br>Last refresh: %3<br>Next refresh: %2").
               arg(usageStr).
               arg(nextRefresh.toString()).
               arg(QDateTime::currentDateTime().toString()));
    Plasma::ToolTipManager::self()->setContent(this, tooltip);
    fetchInProgress = false;
}

void TelemeterPlasmoid::onError(QString errMsg)
{
    qDebug() << "Fetch error";
    fetchInProgress = false;
    meter->setLabel(0, "Fetch error");
    meter->update();
    int nextSecs = 60 * qPow(2, errorCount++);
    qDebug() << "next retry in" << nextSecs / 60 << "mins";
    refreshTimer.start(nextSecs * 1000);
    QDateTime nextRefresh = QDateTime::currentDateTime();
    nextRefresh = nextRefresh.addSecs(nextSecs);
    tooltip.setMainText("Fetch error");
    tooltip.setSubText("Next try: " + nextRefresh.toString() + "<br>" + errMsg + "<br>" + tooltip.subText());
    Plasma::ToolTipManager::self()->setContent(this, tooltip);
}

void TelemeterPlasmoid::onConfigAccepted()
{
    qDebug() << "onConfigAccepted";
    KConfigGroup config = this->config();
    autoRefresh = uiConfig.refreshAutoRB->isChecked();
    config.writeEntry("autoRefresh", autoRefresh);
    displayGB = uiConfig.sizeGBRB->isChecked();
    config.writeEntry("displayGB", displayGB);
    refreshInterval = uiConfig.refreshSB->value();
    config.writeEntry("refreshInterval", refreshInterval);
    login = uiConfig.loginLE->text();
    config.writeEntry("login", login);
    useKWallet = uiConfig.kwalletRB->isChecked();
    config.writeEntry("useKWallet", useKWallet);
    pwd = uiConfig.passwordLE->text();
    if (useKWallet) {
        config.deleteEntry("password");
        openWallet(Write);
    } else {
        config.writeEntry("password", pwd);
    }
    telemeterService.setCredentials(login, pwd);
    setConfigurationRequired(login.isEmpty() || pwd.isEmpty(), i18n("No Telenet credentials configured."));
    if (!configurationRequired()) fetchUsageAsync();
    emit configNeedsSaving();
}

void TelemeterPlasmoid::openWallet(WalletWait wait)
{
    qDebug() << "openWallet" << wait;
    walletWait = wait;
    WId winID = 0;
    if (view()) {
        winID = view()->winId();
    }

    kDebug() << "win id = " << winID;
    wallet = Wallet::openWallet(Wallet::NetworkWallet(),winID , Wallet::Asynchronous);
    if (wait == Read) connect(wallet, SIGNAL(walletOpened(bool)), SLOT(readWallet(bool)));
    else connect(wallet, SIGNAL(walletOpened(bool)), SLOT(writeWallet(bool)));
}

void TelemeterPlasmoid::readWallet(bool ok)
{
    qDebug() << "read wallet" << ok << "login=" << login;
    if (ok && wallet->hasFolder("telemeter-plasmoid")) {
        if (wallet->setFolder("telemeter-plasmoid") && !wallet->readPassword(login, pwd)) {
            qDebug() << "succesfully retrieved pwd from wallet";
            telemeterService.setCredentials(login, pwd);
            fetchUsageAsync();
        } else {
            qDebug() << "problem reading pwd from wallet";
        }
    }
    delete wallet;
    wallet = 0;
}

void TelemeterPlasmoid::writeWallet(bool ok)
{
    qDebug() << "write wallet" << ok;
    if (ok) {
        if (!wallet->hasFolder("telemeter-plasmoid")) {
            wallet->createFolder("telemeter-plasmoid");
        }
        if (wallet->setFolder("telemeter-plasmoid")) {
            wallet->writePassword(login, pwd);
        }
    }
    delete wallet;
    wallet = 0;
    telemeterService.setCredentials(login, pwd);
    fetchUsageAsync();
}

void TelemeterPlasmoid::readConfig()
{
    KConfigGroup config = this->config();
    autoRefresh = config.readEntry("autoRefresh", true);
    displayGB = config.readEntry("displayGB", false);
    refreshInterval = config.readEntry("refreshInterval", 60);
    useKWallet = config.readEntry("useKWallet", false);
    login = config.readEntry("login", "");
    if (useKWallet && !login.isEmpty()) {
        openWallet(Read);
    } else {
        pwd = config.readEntry("password", "");
    }
    if (login.isEmpty() || (pwd.isEmpty() && !useKWallet)) {
        setConfigurationRequired(true, i18n("No Telenet credentials configured."));
    } else telemeterService.setCredentials(login, pwd);
}


K_EXPORT_PLASMA_APPLET(telemeter, TelemeterPlasmoid)

#include "telemeterplasmoid.moc"
