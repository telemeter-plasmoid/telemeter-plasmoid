#ifndef TELEMETERPLASMOID_H
#define TELEMETERPLASMOID_H

#include "telemeterservice.h"
#include "ui_telemeterConfig.h"

#include <plasma/applet.h>
#include <plasma/widgets/meter.h>
#include <plasma/tooltipcontent.h>

#include <KWallet/Wallet>

#include <QGraphicsProxyWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsLinearLayout>
#include <QTimer>

enum WalletWait { None=0, Read, Write };
class TelemeterPlasmoid : public Plasma::Applet
{
    Q_OBJECT
    public:
        TelemeterPlasmoid(QObject *parent, const QVariantList &args);
        ~TelemeterPlasmoid();
        void init();

        void mousePressEvent ( QGraphicsSceneMouseEvent * event );
        void mouseReleaseEvent ( QGraphicsSceneMouseEvent * event );

        void createConfigurationInterface(KConfigDialog *parent);

    public slots:
        void fetchUsageAsync();
        void onUsageFetched(float volumeUsed, float volumeTotal, QString unit);
        void onError(QString errMsg);

        void onConfigAccepted();
    private slots:
        void readWallet(bool);
        void writeWallet(bool);
    private:
        void readConfig();
        void openWallet(WalletWait wait);

        TelemeterService telemeterService;
        Plasma::Meter* meter;
        Plasma::ToolTipContent tooltip;
        Ui::TelemeterConfig uiConfig;
        QString login;
        QString pwd;
        bool useKWallet;
        KWallet::Wallet* wallet;
        WalletWait walletWait;
        int refreshInterval;
        QTimer refreshTimer;
        bool autoRefresh;
        bool displayGB;
        bool fetchInProgress;
        int errorCount;
};

#endif // TELEMETERPLASMOID_H
