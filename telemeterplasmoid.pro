#-------------------------------------------------
#
# Project created by QtCreator 2010-07-26T19:49:38
#
#-------------------------------------------------

QT       += core gui xml

TARGET = telemeterplasmoid
TEMPLATE = app
QMAKE_CXXFLAGS += -DWITH_OPENSSL -DDEBUG
#LIBS += -lgsoap++ -lgsoapssl++
#INCLUDEPATH += /usr/include/gsoap
LIBS += -lssl #-lcrypto -lz
SOURCES += main.cpp\
        telemeterwidget.cpp \
    qneedleindicator.cpp \
    config.cpp \
    gsoap/stdsoap2.cpp \
    gsoap/soapTelemeterServiceSOAPProxy.cpp \
    gsoap/soapC.cpp \
    telemeterservice.cpp \
    telemeterplasmoid.cpp

HEADERS  += telemeterwidget.h \
    qneedleindicator.h \
    config.h \
    gsoap/stdsoap2.h \
    gsoap/telemeter.h \
    gsoap/soapTelemeterServiceSOAPProxy.h \
    gsoap/soapStub.h \
    gsoap/soapH.h \
    telemeterservice.h \
    telemeterplasmoid.h

FORMS    += telemeterwidget.ui \
    telemeterConfig.ui

OTHER_FILES += \
    plasma-applet-telemeter.desktop \
    CMakeLists.txt \
    gsoap/typemap.dat \
    gsoap/TelemeterServiceSOAP.nsmap
