#include "telemeterservice.h"

#include "gsoap/soapTelemeterServiceSOAPProxy.h"

#include <QtDebug>

TelemeterService::TelemeterService()
{
}

void TelemeterService::setCredentials(QString user, QString password)
{
//    qDebug() << "setCredentials" << user << password;
    this->user = user;
    this->pwd = password;
}

void TelemeterService::fetchUsage()
{
    if (user.isEmpty() || pwd.isEmpty()) return;
    qDebug() << "fetchUsage---";
    TelemeterServiceSOAPProxy telemeterService;
    if (soap_ssl_client_context(&telemeterService,
                                SOAP_SSL_SKIP_HOST_CHECK,
                                NULL,	/* keyfile: required only when client must authenticate to server (see SSL docs on how to obtain this file) */
                                NULL,	 /* password to read the key file */
                                NULL, /* cacert file to store trusted certificates (needed to verify server) */
                                NULL,	 /* capath to direcoty with trusted certificates */
                                NULL	 /* if randfile!=NULL: use a file with random data to seed randomness */
                                ))
    {
        soap_print_fault(&telemeterService, stderr);
        emit fetchError(QString("Webservice error:\n"
                                + QString(*soap_faultstring(&telemeterService))) + "\n"
                                + QString(*soap_faultdetail(&telemeterService)));
        return;
    }

    ns1__RetrieveUsageRequestType usageReq;
    QByteArray userBytes = user.toLocal8Bit();
    QByteArray pwdBytes = pwd.toLocal8Bit();
    usageReq.UserId = userBytes.data();
    usageReq.Password = pwdBytes.data();
    ns1__RetrieveUsageResponseType usageResp;
    float volumeUsed, volumeTotal;
    QString unit;
    if (telemeterService.retrieveUsage(&usageReq, &usageResp) == SOAP_OK) {
        qDebug() << "usage = " << usageResp.union_RetrieveUsageResponseType.Volume->TotalUsage;
        volumeTotal = QString(usageResp.union_RetrieveUsageResponseType.Volume->Limit).toInt();
        volumeUsed = QString(usageResp.union_RetrieveUsageResponseType.Volume->TotalUsage).toInt();
        unit = usageResp.union_RetrieveUsageResponseType.Volume->Unit == ns1__UnitType__MB ? "MB" : "GB";
        qDebug() << volumeUsed << "/" << volumeTotal << unit;
        timestampRequest.setTime_t(usageResp.Ticket->Timestamp);
        timestampExpiry.setTime_t(usageResp.Ticket->ExpiryTimestamp);
        qDebug() << "timestamp" << timestampRequest;
        qDebug() << "expiry" << timestampExpiry;
    } else {
        soap_print_fault(&telemeterService, stderr);
        qDebug() << "ERR: " << QString(*soap_faultstring(&telemeterService)) << QString(*soap_faultdetail(&telemeterService));
        emit fetchError(QString("Webservice error:\n"
                                + QString(*soap_faultstring(&telemeterService))) + "\n"
                                + QString(*soap_faultdetail(&telemeterService)));
        return;
    }
    emit usageFetched(volumeUsed, volumeTotal, unit);
}
