#ifndef TELEMETERSERVICE_H
#define TELEMETERSERVICE_H

#include <QObject>
#include <QString>
#include <QDateTime>

class TelemeterService
    :public QObject
{
    Q_OBJECT
public:
    TelemeterService();
    void setCredentials(QString user, QString password);
public slots:
    void fetchUsage();
signals:
    void usageFetched(float volumeUsed, float volumeTotal, QString unit);
    void fetchError(QString errMsg);
public:
    QDateTime timestampRequest, timestampExpiry;
private:
    QString user, pwd;
};

#endif // TELEMETERSERVICE_H
