#include "telemeterwidget.h"
#include "ui_telemeterwidget.h"

#include "telemeterservice.h"

#include <QTimer>
#include <QtDebug>

TelemeterWidget::TelemeterWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TelemeterWidget)
{
    ui->setupUi(this);
    ui->needle->setLabel("test");
    ui->needle->setDigitFormat("%.0f");
    ui->needle->setAnimated(true);
    ui->needle->setBrandOffset(0.4);
    ui->needle->setBrand("TeleMeter");
    ui->needle->setBrandFont(QFont("Serif", 10, QFont::Bold));

    user = "a176960";
    pwd = "RPJRC5";
    connect(ui->testButton, SIGNAL(clicked()), this, SLOT(onTestButton_clicked()));
    connect(this, SIGNAL(usageFetched(float,float,QString)), this, SLOT(updateUsage(float,float,QString)));
    connect(this, SIGNAL(fetchError()), this, SLOT(onFetchError()));
}

TelemeterWidget::~TelemeterWidget()
{
    delete ui;
}

void TelemeterWidget::refresh()
{
    qDebug() << "refresh";
    ui->needle->setValue(0);
    ui->needle->setLabel("Refreshing");
    QTimer::singleShot(0, this, SLOT(fetchUsage()));
}

void TelemeterWidget::onFetchError()
{
    qDebug() << "onFetchError";
    ui->needle->setValue(0);
    ui->needle->setLabel("Error");
}

void TelemeterWidget::updateUsage(float volumeUsed, float volumeTotal, QString unit)
{
    qDebug() << "updateUsage";
        float newval = volumeUsed * 100 /volumeTotal;
        qDebug() << "newval = " << newval;
        ui->needle->setValue(newval);
        ui->needle->setLabel(QString("%1 of %2 %3").arg(volumeUsed).arg(volumeTotal).arg(unit));
}


void TelemeterWidget::onTestButton_clicked()
{
    refresh();
}
