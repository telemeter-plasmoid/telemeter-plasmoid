#ifndef TELEMETERWIDGET_H
#define TELEMETERWIDGET_H

#include <QWidget>

namespace Ui {
    class TelemeterWidget;
}

class TelemeterWidget : public QWidget
{
    Q_OBJECT

public:
    explicit TelemeterWidget(QWidget *parent = 0);
    ~TelemeterWidget();

public slots:
    void fetchUsage();
    void onTestButton_clicked();
    void refresh();
    void updateUsage(float volumeUsed, float volumeTotal, QString unit);
    void onFetchError();
private:
    Ui::TelemeterWidget *ui;
    QString user, pwd;
};

#endif // TELEMETERWIDGET_H
